import 'package:flutter/material.dart';
import 'package:lista_tarefas/tarefa.dart';

void main() {
  runApp(ListaTarefasApp());
}

class ListaTarefasApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(home: new ListaScreen());
  }
}

class ListaScreen extends StatefulWidget {
  State<StatefulWidget> createState() {
    return new ListaScreenState();
  }
}

class ListaScreenState extends State<ListaScreen> {
  List<Tarefa> tarefas = new List<Tarefa>();
  TextEditingController controller = new TextEditingController();

  void adicionaTarefa(String nome) {
    setState(() {
      tarefas.add(Tarefa(nome));
    });

    controller.clear();
  }

  Widget getItem(Tarefa tarefa) {
    return new Row(
      children: [
        IconButton(
          icon: new Icon(
            tarefa.concluida ? Icons.check_box : Icons.check_box_outline_blank,
            color: Colors.black,
          ),
          onPressed: () {
            setState(() {
              tarefa.concluida = !tarefa.concluida;
            });
          },
          iconSize: 42.0,
        ),
        new Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [Text(tarefa.nome), Text(tarefa.data.toIso8601String())],
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Lista de Tarefas"),
        backgroundColor: Colors.black,
      ),
      body: Column(
        children: <Widget>[
          Container(
              padding: EdgeInsets.all(10.0),
              child: TextField(
                controller: controller,
                onSubmitted: adicionaTarefa,
              )),
          Expanded(
              child: ListView.builder(
            itemCount: tarefas.length,
            itemBuilder: (_, index) {
              return getItem(tarefas[index]);
            },
          ))
        ],
      ),
    );
  }
}

class Tarefa {
  String nome;
  DateTime data;
  bool concluida;

  Tarefa(String nome) {
    this.nome = nome;
    this.data = DateTime.now();
    this.concluida = false;
  }

  String get getNome => nome;

  set setNome(String nome) => this.nome = nome;

  bool get getConcluida => concluida;

  set setConcluida(bool concluida) => this.concluida = concluida;

  @override
  String toString() =>
      'Tarefa(nome: $nome, data: $data, concluida: $concluida)';
}
